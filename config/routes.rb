Rails.application.routes.draw do

  devise_for :users, path: "account", path_names: {edit: 'profile'}
  #devise_for :users, path: "auth", path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret', confirmation: 'verification', unlock: 'unblock', registration: 'register', sign_up: 'signup' }
  devise_scope :user do
    get 'login', to: 'devise/sessions#new'
    get 'logout', to: 'devise/sessions#destroy'
  end

  namespace :account do
    resources :dashboard, only: :index
    resources :settings, only: [:edit, :update]
    resources :calls, only: [:index, :show, :update]
    get 'settings' => 'settings#edit'
    resources :voicemails
    resources :phone_numbers
    get '/voicemails/unread', to: 'voicemails#unread'
  end

  resources :voicemails, only: :show # Public resource

  get '/*id' => 'pages#show', as: :page, format: false
  root to: 'pages#show', id: 'home'
end
