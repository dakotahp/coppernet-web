class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  #before_filter :set_locale
  before_filter :get_unlistened_voicemail, only: [:index, :new, :edit, :show]
  layout :set_layout

  private
  def set_layout
    if current_user
      'application'
    else
      'public'
    end
  end

   def after_sign_in_path_for(resource_or_scope)
    if resource_or_scope.is_a?(User)
      account_dashboard_index_path
    else
      super
    end
  end

  def get_unlistened_voicemail
    # TODO: cache this
    if user_signed_in?
      @unlistened_voicemail = Voicemail.unlistened(current_user).desc(:created_at)
    end
  end

  def set_locale
    #I18n.locale = 'US'
  end
end
