class Account::VoicemailsController < ApplicationController
  before_action :set_voicemail, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: :show
  layout :set_layout, only: :show

  def index
    @voicemails = VoicemailDecorator.decorate_collection(Voicemail.where(user_id: BSON::ObjectId.from_string(current_user._id)).desc(:created_at))
  end

  def show
    @voicemail.update_attribute(:listened, 1)
  end

  def update
    @voicemail.update_attributes(voicemail_params)
    if @voicemail.save
      redirect_to account_voicemail_path(@voicemail), flash: {notice: 'Your voicemail has been deleted.'}
    else
      redirect_to account_voicemail_path(@voicemail), flash: {alert: 'Voicemail failed to delete.'}
    end
  end

  def unread
    @voicemails = Call.where(recording_played: 0, user_id: current_user._id)
    respond_to do |format|
      format.json { render :json => @voicemails.to_json }
    end
  end

  def destroy
    @voicemail.destroy
    respond_to do |format|
      format.html { redirect_to account_voicemails_url }
      format.json { head :no_content }
    end
  end

  private
  def set_voicemail
    @voicemail = Voicemail.find(params[:id]).decorate
  end

  def voicemail_params
    params.require(:voicemail).permit(:delete_recording)
  end

  def set_layout
    if current_user
      'application'
    else
      'public'
    end
  end
end
