class Account::CallsController < ApplicationController
  before_action :set_call, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  def index
    @calls = CallDecorator.decorate_collection(Call.where(user_id: BSON::ObjectId.from_string(current_user._id)).desc(:created_at))
  end

  def show
  end

  private
  def set_call
    @call = Call.find(params[:id]).decorate
  end
end
