class Account::DashboardController < ApplicationController
  before_action :authenticate_user!

  def index
    @calls = Call.where(user_id: current_user._id).limit(5)
    @has_twilio_number = PhoneNumber.hasTwilioNumber(current_user)
    @has_forwarding_number = PhoneNumber.hasForwardingNumber(current_user)
  end
end
