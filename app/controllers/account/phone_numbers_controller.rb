class Account::PhoneNumbersController < ApplicationController
  before_action :set_phone_number, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  def index
    @calls = PhoneNumber.where(user_id: current_user._id)
  end

  def create
    @phone_number = PhoneNumber.new(phone_number_params)
    current_user.phone_numbers.push(@phone_number)
    if current_user.save
      redirect_to account_settings_path, flash: {notice: 'Phone number added.'}
    else
      redirect_to account_settings_path, flash: {alert: 'Phone number failed to add.'}
    end
  end

  def update
    current_user.phone_numbers.each do |number|
      number.update_attributes(phone_number_params) if number._id.to_s == params[:id]
    end
    if current_user.save
      redirect_to account_settings_path, flash: {notice: 'Phone number updated.'}
    else
      redirect_to account_settings_path, flash: {alert: 'Phone number failed to update.'}
    end
  end

  def destroy
    @phone_number.delete
    if current_user.save
      redirect_to account_settings_path, flash: {notice: 'Phone number removed.'}
    else
      redirect_to account_settings_path, flash: {alert: 'Phone number failed to delete.'}
    end
  end

  def show
  end

  private
  def set_phone_number
    @phone_number = current_user.phone_numbers.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def phone_number_params
    params.require(:phone_number).permit(:number, :type, :sip_username, :sip_password, :sip_host)
  end
end
