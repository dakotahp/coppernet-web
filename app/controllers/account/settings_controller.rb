class Account::SettingsController < ApplicationController
  before_action :set_setting, only: [:edit, :update]
  before_action :authenticate_user!

  def edit
    # TODO: optimize
    country_codes = YAML.load_file("#{Rails.root}/config/phone_calling_codes_by_country.yml")
    @country_codes = []
    country_codes.each do |key, value|
      value.each do |code|
        @country_codes << ["#{key} (+#{code})", code]
      end
    end

    @phone_numbers = current_user.phone_numbers
    @phone_number = PhoneNumber.new
  end

  def update
    current_user.setting.update_attributes(setting_params)
    if current_user.setting.save
      Time.zone = current_user.setting.time_zone
      redirect_to account_settings_path, flash: {notice: 'Your settings have been updated.'}
    else
      redirect_to account_settings_path, flash: {alert: 'Settings updated successfully.'}
    end
  end

  private
  def set_setting
    if current_user.setting
      @setting = current_user.setting
    else
      @setting = Setting.new
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def setting_params
    params.require(:setting).permit(:notification_email, :twilio_sid, :time_zone, :blacklist_enabled, :sms_forwarding_enabled, :ring_timeout)
  end
end
