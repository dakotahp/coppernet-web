class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  def index
    render :show
  end

  def show
    @phone_number = PhoneNumber.new
    @phone_numbers = current_user.phone_numbers
  end

  def update
    if @user.update_attributes(user_params)
      redirect_to account_dashboard_index_path, flash: {notice: "Your settings have been updated."}
    else
      redirect_to account_dashboard_index_path, flash: {alert: "An error occured and your settings could not be updated."}
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = current_user
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:email)
  end
end
