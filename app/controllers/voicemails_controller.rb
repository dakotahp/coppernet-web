class VoicemailsController < ApplicationController
  before_action :set_voicemail, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: :show
  layout :set_layout, only: :show

  def index
    @calls = Call.all
  end

  def show
  end

  def unread
    @voicemails = Call.where(recording_played: 0, user_id: current_user._id)
    respond_to do |format|
      format.json { render :json => @voicemails.to_json }
    end
  end

  private
  def set_voicemail
    @voicemail = Voicemail.find(params[:id])
  end

  def set_layout
    if current_user
      'application'
    else
      'public'
    end
  end
end
