class VoicemailDecorator < Draper::Decorator
  delegate_all

  def created_at_human
    object.created_at.strftime("%B %d, %Y at %H:%M")
  end

  def created_at_long
    object.created_at.strftime("%B %d, %Y %H:%M")
  end
end
