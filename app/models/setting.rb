class Setting
  include Mongoid::Document
  include Mongoid::Timestamps

  field :notification_email, type: String
  field :blacklist_enabled, type: Integer
  field :sms_forwarding_enabled, type: Integer
  field :twilio_sid, type: String
  field :time_zone, type: String, default: 'UTC'
  field :ring_timeout, type: Integer, default: 20

  embedded_in :user
end
