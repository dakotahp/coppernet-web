class PhoneNumber
  include Mongoid::Document
  field :number, type: Integer
  field :type, type: String
  field :sip_host, type: String
  field :sip_username, type: String
  field :sip_password, type: String

  embedded_in :user

  def self.hasTwilioNumber(user)
    user.phone_numbers.each do |phone_number|
      return true if phone_number.type == 'twilio'
    end
    false
  end

  def self.hasForwardingNumber(user)
    user.phone_numbers.each do |phone_number|
      return true if phone_number.type != 'twilio'
    end
    false
  end
end
