class Call
  include Mongoid::Document

  field :sid, type: String
  field :parent_call_sid, type: String
  field :account_sid, type: String
  field :user_id, type: Moped::BSON::ObjectId
  field :voicemail_id, type: Moped::BSON::ObjectId

  field :from, type: Integer
  field :to, type: Integer
  field :direction, type: String
  field :status, type: String
  field :date_created, type: Date
  field :date_updated, type: Date
  field :caller_name, type: String

  field :start_time, type: Date
  field :end_time, type: Date
  field :duration, type: Integer
  field :price, type: Integer
  field :price_unit, type: Integer

  field :uri, type: String

  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  has_one :voicemail

  def self.find_with_voicemail
    self.where('`recording_url` IS NOT NULL')
  end

  def from_to_human
    return from if from == nil
    localize_number(from, 'us')
  end

  def to_to_human
    return to if to == nil
    localize_number(to, 'us')
  end

  private
  def localize_number(number, locale)
    local_number = number
    case locale
    when 'us'
      local_number = ActionController::Base.helpers.number_to_phone(number.to_s[1..-1], {area_code: true, country_code: number[1], delimiter: ' '})
    else
      local_number = number
    end
    local_number
  end
end
