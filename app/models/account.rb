class Account
  include Mongoid::Document
  field :email, type: String
  field :password, type: String
  field :timezone, type: String
  field :notification_email, type: String
  field :phone_numbers, type: Moped::BSON::ObjectId
  field :blacklist_enabled, type: Integer

  embeds_many :phone_numbers
  accepts_nested_attributes_for :phone_numbers
end
