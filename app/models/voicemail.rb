class Voicemail
  include Mongoid::Document

  field :sid, type: String
  field :call_sid, type: String
  field :call_id, type: Moped::BSON::ObjectId
  field :account_sid, type: String
  field :user_id, type: Moped::BSON::ObjectId

  field :date_created, type: Date
  field :date_updated, type: Date
  field :duration, type: Integer
  field :api_version, type: String
  field :wav_recording_url, type: String
  field :uri, type: String
  field :listened, type: Integer # 0=unlistened, 1=listened
  field :status, type: Integer # 0=undeleted, 1=deleted, 2=archived

  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  belongs_to :call

  before_destroy :delete_voicemail

  def mp3_recording_url
    wav_recording_url + '.mp3'
  end

  def self.unlistened(user)
    where(listened: 0, user_id: user._id)
  end

  def delete_voicemail
    if sid != nil
      @client = Twilio::REST::Client.new 'ACa54968a8e7df4b5188489fdd7f6ce872', 'a37c32f6c8a27264ee0d574ab7c13115'
      @client.account.recordings.get(sid).delete()
    end
  end

end
