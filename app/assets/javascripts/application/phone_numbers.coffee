$('.edit_phone_number #phone_number_type, .new_phone_number #phone_number_type').change(()->
  $parent = $(this).closest('form')
  $details = $parent.find('.sip-details')
  $number_container = $parent.find('.phone_number_container')
  if $(this).val() is 'sip'
    $details.show('fast')
    $number_container.hide()
  else
    $details.hide('fast')
    $number_container.show()
)
