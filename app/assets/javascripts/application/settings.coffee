$('.new_phone_number_link').click((event)->
  event.preventDefault()
  $('.new_phone_number_form_container').toggle('fast')
)

$('form#new_phone_number').submit((event)->
  event.preventDefault()

  $.ajax(
    type:'POST'
    url: $(this).attr('action')
    data: $(this).serialize()
    success: (response)->
      location.reload(true)
    error: (response)->
      alert("Error: #{response}")
  )

  # ajax form
    # on success, hide form with UI and append data to DOM
)
